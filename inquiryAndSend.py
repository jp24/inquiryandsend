import dbus
import time
from bluetooth import *
import os
import bluetooth
from PyOBEX.client import BrowserClient


def proxyobj(bus, path, interface):
    """ commodity to apply an interface to a proxy object """
    obj = bus.get_object('org.bluez', path)
    return dbus.Interface(obj, interface)

def filter_by_interface(objects, interface_name):
    """ filters the objects based on their support
        for the specified interface """
    result = []
    for path in objects.keys():
        interfaces = objects[path]
        for interface in interfaces.keys():
            """ print("  %s interface"% (interface))""" 
            if interface == interface_name:
                result.append(path)
    return result

senddevices = []
bus = dbus.SystemBus()

#Build a proxy for the Adapter
manager = proxyobj(bus, "/", "org.freedesktop.DBus.ObjectManager")
adapter_proxy = bus.get_object("org.bluez", "/org/bluez/hci0")

while 1:
    objects = manager.GetManagedObjects()

    #Call the method StartDiscovery from the adapter api then StopDiscovery
    adapter_proxy.StartDiscovery(dbus_interface="org.bluez.Adapter1")
    time.sleep(10);
    adapter_proxy.StopDiscovery(dbus_interface="org.bluez.Adapter1")

    devices = filter_by_interface(objects, "org.bluez.Device1")
    print("=============")
    for device in devices:
        obj = proxyobj(bus, device, 'org.freedesktop.DBus.Properties')
        devaddress = obj.Get("org.bluez.Device1", "Address")
        print("===Found %s" % (devaddress) )
        services = bluetooth.find_service(uuid="1105", address=devaddress)
        alreadysent = devaddress in senddevices
        if len(services) > 0 and alreadysent == False:
            try:
                first_match = services[0]
                port = first_match["port"]
                print("Sending to %s..." % devaddress)
                client = BrowserClient(devaddress, port)
                client.connect()
                sendret = client.put("Python.txt", "ABC")
                print ("Sending result = %s..." % sendret)
                senddevices.append(devaddress)
                client.disconnect()
            except Exception as e:
                print str(e)
